(ns gce-jenkins-slave-scaler.user
  (:require [mount.lite :refer (defstate) :as mount]
            [clojure.tools.namespace.repl :as repl]
            [clojure.java.classpath :as classpath]
            [clojure.spec.test.alpha :as stest]

            [boardintelligence.gce-jenkins-slave-scaler.config :as config]
            [boardintelligence.gce-jenkins-slave-scaler.gce :as gce]
            [boardintelligence.gce-jenkins-slave-scaler.jenkins :as jenkins]
            [boardintelligence.gce-jenkins-slave-scaler.state-machine :as state-machine]
            ))

(defn go []
  (stest/instrument)
  (mount/start))

(defn reset
  []
  (mount/stop)
  (apply repl/set-refresh-dirs (->> (classpath/classpath-directories)
                                    (map #(.getPath %))))
  (repl/refresh :after 'gce-jenkins-slave-scaler.user/go))
