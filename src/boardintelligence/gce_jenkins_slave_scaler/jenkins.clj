(ns boardintelligence.gce-jenkins-slave-scaler.jenkins
  (:require [mount.lite :refer (defstate) :as mount]
            [boardintelligence.gce-jenkins-slave-scaler.config :as config]
            [org.httpkit.client :as http]
            [clojure.data.json :as json]
            [manifold.deferred :as d]
            [java-time]
            [camel-snake-kebab.core :as csk]
            [clojure.tools.logging :as log]
            [clojure.spec.alpha :as spec]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell])
  (:import (javax.net.ssl X509TrustManager SSLContext TrustManager)
           (java.security.cert X509Certificate)))

(defn- ssl-engine
  []
  (let [tm (reify javax.net.ssl.X509TrustManager
             (getAcceptedIssuers [this] (make-array X509Certificate 0))
             (checkClientTrusted [this chain auth-type])
             (checkServerTrusted [this chain auth-type]))
        client-context (SSLContext/getInstance "TLSv1.2")]
    (.init client-context nil
           (-> (make-array TrustManager 1)
               (doto (aset 0 tm)))
           nil)
    (.createSSLEngine client-context)))


(defn- jenkins-request
  ([url] (jenkins-request url {}))
  ([url options]
   (d/chain
    (http/get url (merge {:basic-auth (config/jenkins-basic-auth)
                          :sslengine (ssl-engine)
                          :timeout 10000
                          :query-params {:depth 2}}
                         options))
    (fn [response]
      (if (= 200 (:status response))
        (json/read-json (:body response))
        (do
          (log/warnf "Did not receive a clean 200 from jenkins! Received: %s" (pr-str response))
          nil))))))


(defn- queue-url
  []
  (format "%s/queue/api/json" (config/jenkins-base-url)))

(def namespace-name (str (ns-name *ns*)))

(defn- fix-kw
  [kw]
  (keyword namespace-name (-> (name kw) csk/->kebab-case)))


(defn- select-and-fix-keys
  [m ks]
  (->> (select-keys m ks)
       (map (fn [[k v]] [(fix-kw k) v]))
       (into {})))

(spec/def ::blocked boolean?)
(spec/def ::buildable boolean?)
(spec/def ::id (spec/and int? pos?))
(spec/def ::in-queue-since java-time/instant?)
(spec/def ::stuck boolean?)
(spec/def ::why string?)
(spec/def ::buildable-start-milliseconds java-time/instant?)
(spec/def ::pending boolean?)

(spec/def queue-item (spec/keys :req [::blocked ::buildable ::id ::in-queue-since ::stuck ::why
                                      ::buildable-start-milliseconds ::pending]))

(defn queue-status
  []
  (d/chain
    (jenkins-request (queue-url))
    (fn [r]
      (if (nil? r)
        r
        (->> (:items r)
             (map (fn [item]
                    (-> item
                        (select-and-fix-keys
                         [:blocked :buildable :id :inQueueSince
                          :stuck :why :buildableStartMilliseconds :pending])
                        (update ::in-queue-since java-time/instant)
                        (update ::buildable-start-milliseconds java-time/instant)))))))))


(defn- computer-url
  []
  (format "%s/computer/api/json" (config/jenkins-base-url)))

(spec/def ::display-name string?)
(spec/def ::hostname string?)
(spec/def ::idle boolean?)
(spec/def ::num-xecutors (spec/and int? pos?))
(spec/def ::offline boolean?)
(spec/def ::offline-cause (spec/or :nil nil? :map map?))
(spec/def ::offline-cause-reason string?)
(spec/def ::temporarily-offline boolean?)

(spec/def ::likely-stuck boolean?)
(spec/def ::number (spec/and int? #(<= 0 %)))
(spec/def ::progress int?)
(spec/def ::executor (spec/keys :req [::idle ::likely-stuck ::number ::progress]))
(spec/def ::executors (spec/coll-of ::executor))

(spec/def ::slave (spec/keys :req [::display-name ::idle ::num-executors ::offline ::offline-cause
                                   ::offline-cause-reason ::temporarily-offline ::executors]))

(spec/fdef slaves-status
           :ret (spec/coll-of ::slave))

(defn slaves-status
  []
  (d/chain
   (jenkins-request (computer-url))
   (fn [r]
     (if (nil? r)
       r
       (->> (:computer r)
            (map (fn [computer]
                   (let [slave (-> computer
                                   (select-and-fix-keys
                                    [:displayName :idle :numExecutors :offline :offlineCause
                                     :offlineCauseReason :temporarilyOffline :executors])
                                   (update ::executors
                                           #(map (fn [e]
                                                   (select-and-fix-keys e [:idle :likelyStuck :number :progress]))
                                                 %)))]
                     (assoc slave ::hostname (-> (clojure.string/split (::display-name slave) #"\.") first)))))
            (filter #(not= "master" (::display-name %)))
            (map (fn [s] [(::hostname s) s]))
            (into {}))))))


(def ^:private local-cli-jar "/tmp/jenkins-cli.jar")

(defn- ensure-cli-jar
  []
  (let [local-file (io/as-file local-cli-jar)]
    (when-not (.exists local-file)
      (log/info "Downloading jenkins-cli.jar..")
      (let [response (http/get (format "%s/jnlpJars/jenkins-cli.jar" (config/jenkins-base-url))
                               {:basic-auth (config/jenkins-basic-auth)
                                :sslengine (ssl-engine)})]
        (assert (= 200 (:status @response))
                (format "Did not receive a clean 200 from jenkins! Received: %s" (pr-str @response)))
        (io/copy (:body @response) local-file)
        (log/info "Download of jenkins-cli.jar succesful.")))))


(defn- base-cli-command
  []
  (format "java -jar %s -s %s -auth %s -logger FINE"
          local-cli-jar
          (config/jenkins-cli-url)
          (clojure.string/join ":" (config/jenkins-basic-auth))))

(defn execute-cli-command
  [ & commands]
  (ensure-cli-jar)
  (let [to-run (format "%s %s" (base-cli-command) (clojure.string/join " " commands))
        _      (log/debugf "Running jenkins cli command: %s" to-run)
        result (apply shell/sh (clojure.string/split to-run #"\s+"))
        clean-exit (zero? (:exit result))]
    (when-not clean-exit
      (log/errorf "Error seen running jenkins cli command '%s' - status %d and output: %s"
                  to-run
                  (:exit result)
                  (:out result)))
    clean-exit))

(defn safely-remove-slave
  [slave-name]
  (execute-cli-command "offline-node" slave-name)
  (execute-cli-command "wait-node-offline" slave-name)
  (execute-cli-command "delete-node" slave-name))
