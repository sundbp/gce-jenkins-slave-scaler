(ns boardintelligence.gce-jenkins-slave-scaler.gce
  (:require [mount.lite :refer (defstate) :as mount]
            [boardintelligence.gce-jenkins-slave-scaler.config :as config]
            [manifold.deferred :as d]
            [java-time]
            [clojure.tools.logging :as log]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as spec])
  (:import [com.google.api.client.googleapis.auth.oauth2 GoogleCredential]
           [com.google.api.client.googleapis.javanet GoogleNetHttpTransport]
           [com.google.api.client.json.jackson2 JacksonFactory]
           [com.google.api.services.compute
            Compute Compute$Builder
            ComputeScopes]
           [com.google.api.services.compute.model
            AccessConfig
            AttachedDisk
            AttachedDiskInitializeParams
            Instance
            Metadata Metadata$Items
            NetworkInterface
            ServiceAccount
            Tags]))


(def ^{:doc "The tag we use to identify our jenkins slaves on GCE."}
  our-instance-tag  "controlled-by-gce-jenkins-scaler")


(defstate transport
  :start (GoogleNetHttpTransport/newTrustedTransport)
  :stop (when @transport
          (.shutdown @transport)))
(alter-meta! #'transport #(assoc % :private true))


(defstate json-factory
  :start (JacksonFactory/getDefaultInstance))
(alter-meta! #'json-factory #(assoc % :private true))


(defn- get-google-credential
  []
  (let [cred (if (config/use-instance-service-account?)
               (GoogleCredential/getApplicationDefault)
               (GoogleCredential/fromStream (io/input-stream (io/resource "service-account.json"))))]
    (-> cred
        (.createScoped [ComputeScopes/COMPUTE]))))


(defstate credential
  :start (get-google-credential))
(alter-meta! #'credential #(assoc % :private true))


(defstate compute
  :start (-> (Compute$Builder. @transport
                               @json-factory
                               @credential)
             (.setApplicationName "gce-jenkins-slave-scaler")
             .build))
(alter-meta! #'compute #(assoc % :private true))


(spec/def ::instance-name string?)
(spec/def ::tag string?)
(spec/def ::tags (spec/coll-of ::tag))
(spec/def ::creation-timestamp inst?)
(spec/def ::instance-status #{"PROVISIONING" "STAGING" "RUNNING" "STOPPING" "STOPPED" "SUSPENDING", "SUSPENDED" "TERMINATED"})
(spec/def ::id (spec/and (partial instance? BigInteger) pos?))

(spec/def ::instance (spec/keys :req [::instance-name ::tags ::creation-timestamp ::instance-status ::id]))

(spec/fdef list-instances
           :args (spec/cat :with-tag ::tag)
           :ret (spec/coll-of ::instance))

(defn list-instances
  [with-tag]
  (let [instances (-> @compute
                      .instances
                      (.list (config/project-id) (config/zone))
                      .execute)]
    (->> (.getItems instances)
         (map (fn [i]
                {::instance-name (.getName i)
                 ::tags (into #{} (-> i .getTags .getItems))
                 ::creation-timestamp (java-time/instant
                                       (java-time/offset-date-time
                                        (get java-time.format/predefined-formatters "iso-zoned-date-time")
                                        (.getCreationTimestamp i)))
                 ::instance-status (.getStatus i)
                 ::id (.getId i)}))
         (filter (fn [i]
                   (contains? (::tags i) with-tag)))
         (map (fn [i] [(::instance-name i) i]))
         (into {}))))


(defn- get-nic
  []
  (let [nic (NetworkInterface.)
        access-config (AccessConfig.)]
    (doto access-config
      (.setType "ONE_TO_ONE_NAT")
      (.setName "External NAT"))
    (doto nic
      (.setNetwork (format "https://www.googleapis.com/compute/v1/projects/%s/global/networks/default"
                           (config/project-id)))
      (.setAccessConfigs [access-config]))))


(defn- disk-type
  []
  (format "https://www.googleapis.com/compute/v1/projects/%s/zones/%s/diskTypes/pd-standard"
          (config/project-id)
          (config/zone)))


(defn- get-disk
  [instance-name]
  (let [disk (AttachedDisk.)
        params (AttachedDiskInitializeParams.)]
    (doto params
      (.setDiskName instance-name)
      (.setSourceImage (config/source-image))
      (.setDiskType (disk-type))
      (.setDiskSizeGb (config/disk-size-gb)))
    (doto disk
      (.setBoot true)
      (.setAutoDelete true)
      (.setType "PERSISTENT")
      (.setInitializeParams params))))


(defn- get-service-account
  []
  (doto (ServiceAccount.)
    (.setEmail "default")
    (.setScopes ["https://www.googleapis.com/auth/devstorage.full_control"
                 "https://www.googleapis.com/auth/compute"])))


(defn- get-metadata
  []
  (let [meta (Metadata.)
        items (->> (config/get-gce-instance-metadata)
                   (map (fn [[k v]]
                          (doto (Metadata$Items.)
                            (.setKey (name k))
                            (.setValue v))))
                   (into []))]
    (doto meta
      (.setItems items))))


(defn- get-tags
  []
  (doto (Tags.)
    (.setItems [our-instance-tag])))


(defn start-instance
  [instance-name]
  (let [instance (Instance.)
        nic (get-nic)
        disk (get-disk instance-name)
        service-account (get-service-account)
        meta-data (get-metadata)
        tags (get-tags)]
    (doto instance
      (.setName instance-name)
      (.setMachineType (format "https://www.googleapis.com/compute/v1/projects/%s/zones/%s/machineTypes/%s"
                               (config/project-id)
                               (config/zone)
                               (config/machine-type)))
      (.setNetworkInterfaces [nic])
      (.setDisks [disk])
      (.setServiceAccounts [service-account])
      (.setMetadata meta-data)
      (.setTags tags))
    (log/infof "Creating instance: %s" (.toPrettyString instance))
    (try
      (let [result (-> @compute
                       .instances
                       (.insert (config/project-id) (config/zone) instance)
                       .execute)
            error (.getError result)]
        (if error
          {:success?  false
           :error-messsage (.toPrettyString error)}
          {:success? true}))
      (catch Throwable t
        {:success? false
         :error-message (.getMessage t)
         :exception t}))))


(defn terminate-instance
  [instance-name]
  (try
    (let [result (-> @compute
                     .instances
                     (.delete (config/project-id) (config/zone) instance-name)
                     .execute)
          error (.getError result)]
      (if error
        {:success?  false
         :error-messsage (.toPrettyString error)}
        {:success? true}))
    (catch Throwable t
      {:success? false
       :error-message (.getMessage t)
       :exception t})))
