(ns boardintelligence.gce-jenkins-slave-scaler.config
  (:require [mount.lite :refer (defstate) :as mount]
            [clojure.java.io :as io]
            [aero.core :as aero]))

(defstate config
  :start (aero/read-config (or (System/getenv "GCE_JENKINS_SLAVE_SCALER_CONFIG_PATH")
                               (io/resource "config.edn"))))


(defn use-instance-service-account?
  []
  (get-in @config [:use-instance-service-account]))


(defn project-id
  []
  (get-in @config [:gce :project-id]))


(defn zone
  []
  (get-in @config [:gce :zone]))


(defn machine-type
  []
  (get-in @config [:gce :machine-type]))


(defn source-image
  []
  (get-in @config [:gce :source-image]))


(defn disk-size-gb
  []
  (get-in @config [:gce :disk-size-gb]))


(defn loop-sleep-time-ms
  []
  (get-in @config [:loop-sleep-time-ms]))


(defn max-num-instances
  []
  (get-in @config [:max-num-instances]))


(defn min-mins-running
  []
  (get-in @config [:min-mins-running]))


(defn max-mins-idle
  []
  (get-in @config [:max-mins-idle]))


(defn max-mins-starting
  []
  (get-in @config [:max-mins-starting]))


(defn max-mins-terminating
  []
  (get-in @config [:max-mins-terminating]))


(defn jenkins-basic-auth
  []
  [(get-in @config [:jenkins :api :username])
   (get-in @config [:jenkins :api :password])])


(defn jenkins-base-url
  []
  (get-in @config [:jenkins :base-url]))


(defn jenkins-cli-url
  []
  (or (get-in @config [:jenkins :cli-url])
      (jenkins-base-url)))


(defn get-gce-instance-metadata
  []
  (get-in @config [:gce :instance-metadata]))
