(ns boardintelligence.gce-jenkins-slave-scaler.state-machine
  (:require [clojure.spec.alpha :as spec]
            [mount.lite :refer (defstate) :as mount]
            [clojure.tools.logging :as log]
            [java-time]
            [random-string.core :as random-string]
            [manifold.deferred :as d]

            [boardintelligence.gce-jenkins-slave-scaler.jenkins :as jenkins]
            [boardintelligence.gce-jenkins-slave-scaler.gce :as gce]
            [boardintelligence.gce-jenkins-slave-scaler.config :as config]))


(spec/def ::jenkins-status #{::starting ::idle ::running ::not-present})

(spec/fdef instance-jenkins-status
           :args (spec/cat :gce-instance ::gce/instance
                           :jenkins-slaves (spec/map-of string? ::jenkins/slave))
           :ret ::jenkins-status)

(defn instance-jenkins-status
  [instance jenkins-slaves]
  (let [hit (get jenkins-slaves (::gce/instance-name instance))]
    (cond
      (and hit (true? (::jenkins/idle hit)))
      ::idle
      (and hit (false? (::jenkins/idle hit)))
      ::running
      (nil? hit)
      ::not-present
      :else
      (throw (ex-info "Inconsistent state observed!" {:gce-instance instance
                                                      :jenkins-slaves jenkins-slaves})))))


(def ^:private up-states   #{"PROVISIONING" "STAGING" "RUNNING"})
(def ^:private down-states #{"STOPPING" "STOPPED" "SUSPENDING", "SUSPENDED" "TERMINATED"})

(defn- find-initial-started
  [instances jenkins-slaves]
  (->> instances
       (filter (fn [[_ info]] (contains? up-states (::gce/instance-status info))))
       (filter (fn [[instance-name _]] (not (get jenkins-slaves instance-name))))
       (map (fn [[instance-name info]]
              [instance-name (assoc info ::first-seen-starting (java-time/instant))]))
       (into {})))


(defn- find-initial-terminated
  [instances]
  (->> instances
       (filter (fn [[_ info]] (contains? down-states (::gce/instance-status info))))
       (map (fn [[instance-name info]]
              [instance-name (assoc info ::first-seen-terminating (java-time/instant))]))
       (into {})))


(defn- initial-state
  []
  (let [instances  (gce/list-instances gce/our-instance-tag)
        jenkins-slaves @(jenkins/slaves-status)]
    (assert (and instances jenkins-slaves) "Could not initialize state - bad inputs!")
    (let [started    (find-initial-started instances jenkins-slaves)
          terminated (find-initial-terminated instances)
          running    (->> instances
                          (filter (fn [[hostname _]]
                                    (and (not (or (get started hostname)
                                                  (get terminated hostname)))
                                         (get jenkins-slaves hostname)))))]
      {:started    started
       :running    (->> running
                        (map (fn [[hostname info]]
                               (let [status (instance-jenkins-status info jenkins-slaves)]
                                 [hostname
                                  (if (= ::idle status)
                                    (merge info {::jenkins-status status
                                                 ::first-seen-idle (java-time/instant)})
                                    (assoc info ::jenkins-status status))])))
                        (into {}))
       :terminated terminated})))


(defn- not-member-of-both
  [group1 group2]
  (->> group1
       (filter (fn [[hostname _]] (not (contains? group2 hostname))))
       (into {})))


(defn- member-of-both
  [group1 group2]
  (->> group1
       (filter (fn [[hostname _]] (contains? group2 hostname)))
       (into {})))


(defn- find-unfinished-starts
  [started slaves]
  (not-member-of-both started slaves))


(defn- find-unfinished-terminations
  [terminated instances]
  (member-of-both terminated instances))


(defn- find-overdue-terminations
  [terminated]
  (let [now (java-time/instant)]
    (->> terminated
         (filter (fn [[_ info]]
                   (java-time/after? now
                                     (java-time/plus (::first-seen-terminating info)
                                                     (java-time/minutes (config/max-mins-terminating))))))
         (into {}))))


(defn- find-overdue-starts
  [started]
  (let [now (java-time/instant)]
    (->> started
         (filter (fn [[_ info]]
                   (java-time/after? now
                                     (java-time/plus (::first-seen-starting info)
                                                     (java-time/minutes (config/max-mins-starting))))))
         (into {}))))


(defn- remove-overdue-starts
  [remaining-starts overdue-starts]
  (->> remaining-starts
       (filter (fn [[instance-name _]]
                 (not (contains? overdue-starts instance-name))))
       (into {})))


(defn- remove-terminated-slaves
  [slaves terminated]
  (not-member-of-both slaves terminated))


(defn- get-instance-state
  [state instance]
  (let [instance-name (::gce/instance-name instance)]
    (or (get (:started state) instance-name)
        (get (:running state) instance-name)
        (get (:terminated state) instance-name))))


(defn update-instance
  [prev-state jenkins-slaves [instance-name instance]]
  (let [prev-instance-state  (get-instance-state prev-state instance)
        prev-jenkins-status  (or (::jenkins-status prev-instance-state) ::not-present)
        new-jenkins-status   (instance-jenkins-status instance jenkins-slaves)]
    [instance-name
     (case new-jenkins-status
       ::running
       (-> instance
           (assoc  ::jenkins-status ::running)
           (dissoc ::first-seen-idle ::first-seen-starting))

       ::idle
       (-> (if (= ::idle prev-jenkins-status)
             (merge instance {::jenkins-status ::idle
                              ::first-seen-idle (::first-seen-idle prev-instance-state)})
             (merge instance {::jenkins-status ::idle
                              ::first-seen-idle (java-time/instant)}))
           (dissoc ::first-seen-starting))

       ::not-present
       (if (= ::starting prev-jenkins-status)
         (merge instance {::jenkins-status ::starting
                          ::first-seen-starting (::first-seen-starting prev-instance-state)})
         (assoc instance ::jenkins-status ::not-present))

       :else
       (throw (ex-info "Inconsistent state observed!" {:gce-instance instance
                                                       :prev-jenkins-status prev-jenkins-status
                                                       :new-jenkins-status new-jenkins-status})))]))

(defn update-running-instances
  [instances prev-state jenkins-slaves]
  (->> instances
       (map (partial update-instance prev-state jenkins-slaves))
       (into {})))


(defn- find-unidentified-instances
  [instances remaining-starts running-instances remaining-terminations]
  (->> instances
       (filter (fn [[instance-name _]]
                 (not (or (contains? remaining-starts instance-name)
                          (contains? running-instances instance-name)
                          (contains? remaining-terminations instance-name)))))
       (map (fn [[instance-name info]] [instance-name (assoc info ::jenkins-status ::not-present)]))
       (into {})))


(defn- terminate-instance?
  [jenkins-queue [_ instance-state]]
  (let [now (java-time/instant)
        jenkins-status (::jenkins-status instance-state)
        queue-length (count jenkins-queue)]
    ;; first see if instance has run for min-mins-running
    (if (java-time/before? now
                           (java-time/plus (::gce/creation-timestamp instance-state)
                                           (java-time/minutes (config/min-mins-running))))
      false
      (or
       ;; always terminate unknown instances
       (= ::not-present jenkins-status)

       ;; if queue is empty, instance is idle and has been idle for max-mins-idle we shut it down
       (and (= ::idle jenkins-status)
            (= 0 queue-length)
            (java-time/after? now
                              (java-time/plus (::first-seen-idle instance-state)
                                              (java-time/minutes (config/max-mins-idle)))))

       ;; if instance is starting and hasn't managed to start in max-mins-starting it's not booting ok
       ;; so we shut it down
       (and (= ::starting jenkins-status)
            (java-time/after? now
                              (java-time/plus (::first-seen-starting instance-state)
                                              (java-time/minutes (config/max-mins-starting)))))))))


(defn find-instances-to-terminate
  [instances jenkins-queue]
  (->> instances
       (filter (partial terminate-instance? jenkins-queue))
       (map (fn [[instance-name info]]
              [instance-name (-> info
                                 (assoc ::first-seen-terminating (java-time/instant)
                                        ::jenkins-status ::not-present)
                                 (dissoc ::first-seen-idle ::first-seen-starting))]))
       (into {})))


(defn- remove-instances-to-terminate
  [instances to-terminate]
  (not-member-of-both instances to-terminate))


(defn- count-in-status
  [status instances]
  (let [hits (filter (fn [[_ state]]
                       (= status (::jenkins-status state)))
                     instances)]
    (count hits)))


(defn- random-instance-name
  []
  (format "gce-jenkins-slave-scaler-%s" (random-string/string 5)))


(defn instances-to-start
  [running-instances remaining-starts jenkins-queue]
  (let [num-idle     (count-in-status ::idle running-instances)
        num-running  (count-in-status ::running running-instances)
        num-starting (count remaining-starts)
        num-in-queue (count jenkins-queue)
        num-to-start (max 0
                          (min (- num-in-queue num-idle num-starting)
                               (- (config/max-num-instances) num-running num-idle num-starting)))]
    (log/debugf "num-starting: %d | num-idle: %d | num-running: %d | jobs-in-queue: %d | num-to-start: %d"
                num-starting num-idle num-running num-in-queue num-to-start)
    (->> (range num-to-start)
         (map (fn [_]
                (let [instance-name (random-instance-name)]
                  [instance-name
                   {::gce/instance-name instance-name
                    ::jenkins-status ::starting
                    ::first-seen-starting (java-time/instant)}])))
         (into {}))))


(defn- report-overdue-terminations
  [overdue-terminations]
  (doseq [[instance-name _] overdue-terminations]
    (log/warnf "Instance %s didn't terminate within the alloted time frame! (will try to terminate it again)"
               instance-name)))


(defn- report-overdue-starts
  [overdue-starts]
  (doseq [[instance-name _] overdue-starts]
    (log/warnf "Instance %s didn't start within the alloted time frame! (will terminate it to try again)"
               instance-name)))


(defn- terminate-instances
  [to-terminate jenkins-slaves]
  (doseq [instance-name (keys to-terminate)]
    (let [slave-info (get jenkins-slaves instance-name)]
      (when slave-info
        (jenkins/safely-remove-slave (::jenkins/display-name slave-info)))))
  (let [results (map (fn [instance-name]
                       [instance-name (future (gce/terminate-instance instance-name))])
                     (keys to-terminate))
        failed (filter (fn [[_ result]] (not (:success? @result))) results)]
    (doseq [[instance-name result] failed]
      (if (:exception result)
        (log/warnf (:exception result)
                   "Error terminating instance %s: %s" instance-name (:error-message @result))
        (log/warnf "Error terminating instance %s: %s" instance-name (:error-message @result))))
    (->> results
         (filter (fn [[_ result]] (:success? @result)))
         (map (fn [[instance-name _]]
                [instance-name (get to-terminate instance-name)]))
         (into {}))))


(defn- start-instances
  [to-start]
  (let [results (map (fn [instance-name]
                       [instance-name (future (gce/start-instance instance-name))])
                     (keys to-start))
        failed (filter (fn [[_ result]] (not (:success? @result))) results)]
    (doseq [[instance-name result] failed]
      (if (:exception result)
        (log/warnf (:exception result)
                   "Error starting instance %s: %s" instance-name (:error-message @result))
        (log/warnf "Error starting instance %s: %s" instance-name (:error-message @result))))
    (->> results
         (filter (fn [[_ result]] (:success? @result)))
         (map (fn [[instance-name _]]
                [instance-name (get to-start instance-name)]))
         (into {}))))


(defn- update-termination-info
  [instances]
  (->> instances
       (map (fn [[instance-name info]]
              [instance-name (assoc info ::first-seen-terminating (java-time/instant))]))
       (into {})))


(defn- update-state
  [prev-state]
  (log/debugf "Performing 1 iteration of the update loop (%s)" (java-time/instant))
  (log/debug (with-out-str (clojure.pprint/pprint prev-state)))
  (let [instances      (gce/list-instances gce/our-instance-tag)
        jenkins-slaves (jenkins/slaves-status)
        jenkins-queue  (jenkins/queue-status)]
    (if (or (nil? instances)
            (nil? @jenkins-slaves)
            (nil? @jenkins-queue))
      nil
      (let [remaining-starts       (find-unfinished-starts (:started prev-state) @jenkins-slaves)
            remaining-terminations (find-unfinished-terminations (:terminated prev-state) instances)

            overdue-terminations   (find-overdue-terminations remaining-terminations)
            overdue-starts         (find-overdue-starts remaining-starts)

            remaining-starts       (remove-overdue-starts remaining-starts overdue-starts)

            living-slaves          (remove-terminated-slaves @jenkins-slaves (:terminated prev-state))

            running-instances      (update-running-instances (member-of-both instances living-slaves)
                                                             prev-state
                                                             living-slaves)
            unidentified           (find-unidentified-instances instances
                                                                remaining-starts
                                                                running-instances
                                                                remaining-terminations)

            to-terminate           (find-instances-to-terminate running-instances @jenkins-queue)
            running-instances      (remove-instances-to-terminate running-instances to-terminate)

            to-start               (instances-to-start running-instances remaining-starts @jenkins-queue)

            terminated             (terminate-instances (merge to-terminate overdue-terminations overdue-starts unidentified) @jenkins-slaves)
            started                (start-instances to-start)]
        (report-overdue-terminations overdue-terminations)
        (report-overdue-starts overdue-starts)
        (log/debugf "num-terminating: %d" (count (merge remaining-terminations terminated)))
        {:running    running-instances
         :started    (merge remaining-starts started)
         :terminated (merge remaining-terminations (update-termination-info terminated))}))))


(defn- state-update-loop
  [start-state]
  (let [sleep-time (config/loop-sleep-time-ms)]
    (log/info "Starting infinite state update loop..")
    (loop [state start-state]
      (let [updated-state (try
                            (update-state state)
                            (catch Throwable t
                              (log/error t "state-update-loop caught exception!")
                              nil))]
        (Thread/sleep sleep-time)
        (recur (or updated-state state))))))


(defstate update-loop
  :start (future (try
                   (state-update-loop (initial-state))
                   (catch Throwable t
                     (when-not (instance? InterruptedException t)
                       (log/error t "Caught exception in unexpected place!")))))
  :stop (when @update-loop
          (log/info "Cancelling state-update-loop..")
          (future-cancel @update-loop)))
