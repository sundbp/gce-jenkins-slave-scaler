(ns boardintelligence.gce-jenkins-slave-scaler.main
  (:gen-class)
  (:require [clojure.tools.logging :as log]
            [mount.lite :refer (defstate) :as mount]

            ;; Any NS using defstate needs to be referenced here
            ;; explicitly or transitively
            [boardintelligence.gce-jenkins-slave-scaler.state-machine :as state-machine]))

(defn- on-shutdown! [callback]
  (.addShutdownHook (Runtime/getRuntime)
                    (Thread. callback)))

(defn- wait-for-exit! []
  (.join (Thread/currentThread)))


(defn -main
  [ & args]
  ;; perform any work needed to be done BEFORE we start the system here!
  (log/info ::-main "starting gce-jenkins-slave-scaler system..")
  (mount/start)
  (on-shutdown! #(do (log/info ::-main "stopping gce-jenkins-slave-scaler system...")
                     (mount/stop)
                     (log/info ::-main "system stopped successfully.")))
  (log/info ::-main "system started successfully.")
  ;; perform any work needed to be done AFTER the system has started here!
  (wait-for-exit!))
