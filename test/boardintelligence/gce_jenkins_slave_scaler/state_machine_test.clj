(ns boardintelligence.gce-jenkins-slave-scaler.state-machine-test
  (:require [boardintelligence.gce-jenkins-slave-scaler.state-machine :as sut]
            [clojure.test :as t]
            [clojure.spec.alpha :as spec]
            [clojure.spec.test.alpha :as spec-test]
            [clojure.spec.gen.alpha :as gen]
            [boardintelligence.gce-jenkins-slave-scaler.gce :as gce]
            [boardintelligence.gce-jenkins-slave-scaler.jenkins :as jenkins]))

(spec-test/instrument)

(def generators {})

;; TODO: work out which parts are reasonable to test
