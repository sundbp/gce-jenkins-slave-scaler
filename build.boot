(def project 'boardintelligence/gce-jenkins-slave-scaler)
(def version "0.1.0-SNAPSHOT")

(task-options!
 pom {:project     project
      :version     version
      :description "Autoscaler for Jenkins CI Slaves running on Google Compute Engine."
      :url         "https://bitbucket.com/boardiq/gce-jenkins-slave-scaler"
      :scm         {:url "https://bitbucket.com/boardiq/gce-jenkins-slave-scaler"}
      :license     {"MIT" "https://opensource.org/licenses/MIT"}}
 push {:ensure-branch nil
       :ensure-clean nil})


;; common dependencies
(merge-env! :dependencies '[[org.clojure/clojure "1.9.0-alpha19"]
                            [adzerk/boot-test "1.1.2" :scope "test"]
                            [sundbp/booting "0.1.8"]
                            [org.clojure/test.check "0.9.0"]
                            [org.clojure/data.json "0.2.6"]
                            [clojure.java-time "0.3.0"]
                            [http-kit "2.2.0"]
                            [functionalbytes/mount-lite "2.0.0-SNAPSHOT"]
                            [aero "1.1.2"]
                            [manifold "0.1.7-alpha5"]
                            [com.google.cloud/google-cloud-compute "0.22.0-alpha"]
                            [random-string "0.1.0"]
                            [camel-snake-kebab "0.4.0"]
                            ;; Logging
                            [org.clojure/tools.logging "0.4.0"]]
            :resource-paths #{"src"}
            :source-paths   #{"resources"})

;; ensure we have the tasks needed for our build.boot file available
(require '[adzerk.boot-test :refer :all])
(require '[sundbp.booting :as booting])


(def merge-with-vec
  (partial
   merge-with
   (comp vec concat)))


(defn merge-map! [m]
  (->> m
       seq
       (apply concat)
       vec
       (apply merge-env!)))


;;;;;; dev repl fns

(defn dev-env [deployment]
  "no cidr, repl bits"
  {:resource-paths #{"dev"}
   :dependencies '[[org.clojure/tools.namespace "0.2.10"]
                   [org.clojure/java.classpath "0.2.3"]]
   :repl-options {:init-ns (symbol (str deployment ".user"))}})


(defn dev [deployment port]
  (merge-map! (dev-env deployment))
  (merge-map! {:dependencies '[[org.clojure/tools.nrepl "0.2.12"]
                               [refactor-nrepl "2.4.0-SNAPSHOT"]
                               [cider/cider-nrepl "0.15.1-SNAPSHOT"]]})
  (println (str "Starting nREPL on 0.0.0.0:" port))
  (comp (javac)
        (repl :server true
              :middleware '[refactor-nrepl.middleware/wrap-refactor
                            cider.nrepl/cider-middleware]
              :bind "0.0.0.0"
              :port port
              :init-ns (symbol (str deployment ".user")))
        (wait)))


;;;;; common dependency parts

(def logback
  {:dependencies '[[ch.qos.logback/logback-classic "1.2.2" :exclusions [org.slf4j/slf4j-api]]
                   [org.slf4j/jul-to-slf4j "1.7.25"]
                   [org.slf4j/jcl-over-slf4j "1.7.25"]
                   [org.slf4j/log4j-over-slf4j "1.7.25"]]})


(def logback-for-test
  {:dependencies '[[ch.qos.logback/logback-classic "1.2.2" :scope "test" :exclusions [org.slf4j/slf4j-api]]
                   [org.slf4j/jul-to-slf4j "1.7.25" :scope "test"]
                   [org.slf4j/jcl-over-slf4j "1.7.25" :scope "test"]
                   [org.slf4j/log4j-over-slf4j "1.7.25" :scope "test"]]})


;;;;; gce-jenkins-slave-scaler

(def gce-jenkins-slave-scaler-env
  (merge-with-vec
   logback))

(deftask merge-gce-jenkins-slave-scaler-env!
  []
  (merge-map! gce-jenkins-slave-scaler-env)
  identity)

(deftask gce-jenkins-slave-scaler-repl []
  (merge-gce-jenkins-slave-scaler-env!)
  (dev "gce-jenkins-slave-scaler" 6780))

(deftask gce-jenkins-slave-scaler-uberjar []
  (merge-gce-jenkins-slave-scaler-env!)
  (comp
   (booting/standalone :main 'boardintelligence.gce-jenkins-slave-scaler.main-aot)
   (target)))

(deftask gce-jenkins-slave-scaler-test []
  (merge-gce-jenkins-slave-scaler-env!)
  (test))
