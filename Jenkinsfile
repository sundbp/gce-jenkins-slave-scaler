properties([[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactNumToKeepStr: '10', numToKeepStr: '10']]])

def withRetry(String script) {
    timeout(time: 10, unit: 'MINUTES') {
        waitUntil {
            def r = sh(script: script, returnStatus: true)
            return (r == 0)
        }
    }
}

node('docker') {
    stage(name: 'checkout repo') {
        timeout(time: 10, unit: 'MINUTES') {
            waitUntil {
                try {
                    checkout scm
                    return true
                } catch (exception) {
                    return false
                }
            }
        }
        env.GIT_SHA = sh(returnStdout: true, script: 'git rev-parse HEAD').trim().take(7)
    }
    stage(name: "run tests") {
        ansiColor('xterm') {
            withRetry "gcloud docker -- pull gcr.io/jenkins-ci-bi-1/clojure-base:latest"
            sh "docker run --rm -v \$(pwd):/app gcr.io/jenkins-ci-bi-1/clojure-base:latest boot gce-jenkins-slave-scaler-test"
        }
    }
    if (env.BRANCH_NAME == 'master') {
        stage(name: 'build image') {
            ansiColor('xterm') {
                withRetry "gcloud docker -- pull gcr.io/jenkins-ci-bi-1/clojure-base:latest"
                sh(returnStatus: true, script: "docker inspect gce-slave-scaler-builder && docker rm -f gce-slave-scaler-builder")
                sh "docker run --name gce-slave-scaler-builder -v \$(pwd):/app gcr.io/jenkins-ci-bi-1/clojure-base:latest boot gce-jenkins-slave-scaler-uberjar"
                sh "docker cp gce-slave-scaler-builder:/app/target/gce-jenkins-slave-scaler-0.1.0-SNAPSHOT-standalone.jar standalone.jar"
                sh "chmod 644 standalone.jar"
                sh "docker rm gce-slave-scaler-builder"
                withRetry "gcloud docker -- pull gcr.io/jenkins-ci-bi-1/gce-jenkins-slave-scaler:latest"
                sh "gcloud docker -- build --cache-from gcr.io/jenkins-ci-bi-1/gce-jenkins-slave-scaler:latest -t gcr.io/jenkins-ci-bi-1/gce-jenkins-slave-scaler:${env.GIT_SHA} -f docker/prod/Dockerfile ."
                withRetry "gcloud docker -- push gcr.io/jenkins-ci-bi-1/gce-jenkins-slave-scaler:${env.GIT_SHA}"
                sh "docker tag gcr.io/jenkins-ci-bi-1/gce-jenkins-slave-scaler:${env.GIT_SHA} gcr.io/jenkins-ci-bi-1/gce-jenkins-slave-scaler:latest"
                withRetry "gcloud docker -- push gcr.io/jenkins-ci-bi-1/gce-jenkins-slave-scaler:latest"
            }
        }
    }
}
