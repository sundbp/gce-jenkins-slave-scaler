#!/bin/bash

secrets=$(curl --fail "http://metadata.google.internal/computeMetadata/v1/project/attributes/secrets-edn" -H "Metadata-Flavor: Google")
if [[ $? -eq 0 ]];then
    echo "Using secrets fetched via project metadata."
    echo $secrets > "$HOME/.secrets.edn"
fi
config=$(curl --fail "http://metadata.google.internal/computeMetadata/v1/project/attributes/scaler-config-edn" -H "Metadata-Flavor: Google")
if [[ $? -eq 0 ]];then
    echo "Using config.edn file fetched via project metadata."
    echo $config > "/app/config.edn"
fi

exec java -DAPP_NAME="$APP_NAME" -Dlogback.configurationFile=/app/logback.xml \
-XX:-OmitStackTraceInFastThrow \
-XX:+TieredCompilation \
-XX:+HeapDumpOnOutOfMemoryError \
-XX:HeapDumpPath=/app \
-XX:+PrintGCDateStamps \
-verbose:gc \
-XX:+PrintGCDetails \
-Xloggc:/app/gc.log \
-XX:+UseGCLogFileRotation \
-XX:NumberOfGCLogFiles=10 \
-XX:GCLogFileSize=100M \
 $JVM_OPTS \
-jar /app/standalone.jar
